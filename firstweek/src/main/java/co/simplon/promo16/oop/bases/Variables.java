package co.simplon.promo16.oop.bases;

import java.util.ArrayList;
import java.util.List;


public class Variables {
public void first() {
    int age = 31;
    String promoName = "promo 16";
    boolean likesJava = true;
    List<String>languagesKnown = new ArrayList<>(List.of("Swift", "Java", "HTML"));

    System.out.println(age);
    System.out.println(likesJava);
    System.out.println(languagesKnown);

}

public void withParameters(String parameter1, int parameter2) {
System.out.println(parameter1 + parameter2);
}

public String withReturn() {
    return "Le return";
}
}
