package co.simplon.promo16.oop.bases;

import java.util.ArrayList;
import java.util.List;

public class Looping {

    public void loop(int row) {

        String message = "ë";
        for (int i = 0; i <= row; i++) {

            System.out.println(message);
            message += "ehe";
        }

    }



    public int findMax(List<Integer> numbers) {
        
        int maxNumber = numbers.get(0);
        for (int i = 1; i < numbers.size(); i++) {
            
            if (numbers.get(i) > maxNumber) {
                
                maxNumber = numbers.get(i);
            }
        }
        return maxNumber;
    }
}
