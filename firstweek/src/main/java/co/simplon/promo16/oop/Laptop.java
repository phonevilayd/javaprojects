package co.simplon.promo16.oop;

public class Laptop {
    public String model;
    public int battery;
    public boolean turnedOn;
    public boolean pluggedIn;
    public String OS;

    public Laptop(String model, int battery, boolean turnedOn, boolean pluggedIn) {
        this.model = model;
        this.battery = battery;
        this.turnedOn = turnedOn;
        this.pluggedIn = pluggedIn;
        this.OS = OS;
    }

    public void plug() {
        pluggedIn = true;
        battery += 5;

    }

    public void powerSwitch() {
        if (turnedOn == true) {
            turnedOn = false;
        } else if (turnedOn == false && pluggedIn == true) {
            turnedOn = true;
        } else if (turnedOn == false && pluggedIn == false && battery >= 10) {
            turnedOn = true;
            battery -= 5;
        } else {
            System.out.println("Does nothing");
        }
    }

    public void install(String OS) {
        if (turnedOn == true && pluggedIn == false) {
            battery -= 5;
        }
    }
    private void consumeBattery () {
        if (pluggedIn == false) {
            battery -= 5;
        } else if (pluggedIn == true && battery <= 95) {
            battery += 5;
        } else if (battery == 0) {
            turnedOn = false;
        } 
    }


}
