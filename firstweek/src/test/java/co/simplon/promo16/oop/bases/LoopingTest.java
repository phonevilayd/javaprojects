package co.simplon.promo16.oop.bases;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

public class LoopingTest {
    @Test
    public void testFindMax() {

        Looping testLooping = new Looping();

        int maxNumber = 9;

        int result = testLooping.findMax(List.of(1,2,3,4,2,4,9,1,3));

        assertEquals(maxNumber, result);
    }
}
