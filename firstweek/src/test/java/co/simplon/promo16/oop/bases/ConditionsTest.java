package co.simplon.promo16.oop.bases;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ConditionsTest {
    @Test
    public void testSkynetIA(String string) {
    Conditions instance = new Conditions();
    String result = instance.skynetIA("How are you ?");

    assertEquals("Hello how are you ?", result);
    assertEquals("I'm fine, thank you", result);
    assertEquals("Goodbye, friend", result);
    assertEquals("I don't understand", result);

    }

}
